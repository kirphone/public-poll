package org.apluscorp.publicpoll;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Validator {

    Tools tool;

    public Validator(Context context){
        tool = new Tools(context);
    }

    public boolean validateName(final EditText name, DatabaseReference usernames) {
        final boolean[] valid = {true};
        String nameText = name.getText().toString();

        if(TextUtils.isEmpty(nameText)) {
            tool.setError(name, true);
            valid[0] = false;
        } else {
            tool.setError(name, false);
        }

        if(!valid[0])
            return valid[0];


        usernames.child(nameText).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    tool.setError(name, true);
                    valid[0] = false;
                } else {
                    tool.setError(name, false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return valid[0];
    }

    public boolean validateEmail(final EditText email, FirebaseAuth mAuth) {
        final boolean[] valid = {true};
        String emailText = email.getText().toString();

        if(TextUtils.isEmpty(emailText)) {
            tool.setError(email, true);
            valid[0] = false;
        } else {
            tool.setError(email, false);
        }

        if(!valid[0])
            return valid[0];


        //проверка email на уникальность
        mAuth.fetchProvidersForEmail(emailText).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                if(task.isSuccessful()){
                    if(task.getResult().getProviders().size() == 1){
                        valid[0] = false;
                        tool.setError(email, true);
                    } else{
                        tool.setError(email, false);
                    }
                }
            }
        });

        return valid[0];
    }

    public boolean validatePassword(EditText password) {
        boolean valid = true;
        String passwordText = password.getText().toString();

        if(TextUtils.isEmpty(passwordText)) {
            tool.setError(password, true);
            valid = false;
        } else if(passwordText.length() < 6) {
            tool.setError(password, true);
            valid = false;
        } else {
            tool.setError(password, false);
        }

        return valid;
    }

    public boolean validatePhone(EditText phone, boolean canBeEmpty){

        boolean valid = true;
        String number = phone.getText().toString();
        if(TextUtils.isEmpty(number) && canBeEmpty){
            tool.setError(phone, false);
            return true;
        }
        if(number.startsWith("+7")){
            if(number.length() != 12){
                valid = false;
                tool.setError(phone, true);
            } else{
                tool.setError(phone, false);
            }
        } else if(number.length() == 11){
             valid = true;
             tool.setError(phone, false);
        } else {
            valid = false;
            tool.setError(phone, true);
        }

        return valid;
    }

    public boolean validateConfPassword(EditText confPassword, EditText password) {
        if(!validatePassword(password))
            return false;

        boolean valid = true;
        String confPasswordText = confPassword.getText().toString();

        if(TextUtils.isEmpty(confPasswordText) || password.getText().toString().compareTo(confPasswordText) != 0) {
            tool.setError(confPassword, true);
            valid = false;
        } else {
            tool.setError(confPassword, false);
        }

        return valid;
    }

    public boolean validateChecker(CheckBox checked){
        boolean valid = true;

        if(!checked.isChecked()) {
            tool.setError(checked, true);
            valid = false;
        } else {
            tool.setError(checked, false);
        }
        return valid;
    }
}
