package org.apluscorp.publicpoll;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Кирилл on 07.12.2017.
 */

public class WelcomeActivity extends AppCompatActivity {

    private static final String TAG = "Auth";
    private Timer openNextActivityTimer;
    private OpenNextActivityTimerTask openNextActivityTimerTask;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // открытие следующего активити через время
        openNextActivityTimer = new Timer();
        openNextActivityTimerTask = new OpenNextActivityTimerTask();
        mAuth = FirebaseAuth.getInstance();
    }

    protected void openNextActivity()
    {
        startActivity(new Intent(this, AuthActivity.class));
        finish();         // удаляем из памяти текущее активити
    }

    class OpenNextActivityTimerTask extends TimerTask{

        @Override
        public void run() {
            openNextActivity();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            openNextActivityTimer.schedule(openNextActivityTimerTask, 1000);
        }
    }
}
