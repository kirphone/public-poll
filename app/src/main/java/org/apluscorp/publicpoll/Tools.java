package org.apluscorp.publicpoll;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.view.View;

/**
 * Created by Кирилл on 15.12.2017.
 */

public class Tools {

    private Context context;

    public Tools(Context _context) {
        context = _context;
    }

    public void setError(View v, boolean isRed) {
        v.getBackground().setColorFilter(context.getResources().getColor(isRed ? R.color.colorRed : R.color.colorBlack), PorterDuff.Mode.SRC_ATOP);
    }
}
