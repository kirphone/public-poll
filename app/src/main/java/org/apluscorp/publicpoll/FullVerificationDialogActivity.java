package org.apluscorp.publicpoll;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

/**
 * Created by Кирилл on 19.03.2018.
 */

public class FullVerificationDialogActivity extends AppCompatActivity{

    private static final String TAG = "Verification";
    @BindView(R.id.phoneVerificationCodeField) EditText code;
    String phone, verificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullverificationdialog);
        ButterKnife.bind(this);
        this.setFinishOnTouchOutside(false);

        phone = getIntent().getStringExtra("phoneNumber");
        initializeVerificationPhoneCallback();
        user = FirebaseAuth.getInstance().getCurrentUser();

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks
        );
    }

    @OnTextChanged(R.id.phoneVerificationCodeField)
    public void onCodeChanged(CharSequence s, int start, int before, int count) {
        if(s.length() != 6 || verificationId == null)
            return;

        linkWithCredential(PhoneAuthProvider.getCredential(verificationId, (String) s));
    }

    private void linkWithCredential(PhoneAuthCredential phoneAuthCredential){
        user.linkWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "linkWithCredential:success");

                    updateUI();
                } else {
                    Exception e = task.getException();
                    Log.d(TAG, "linkWithCredential:failure", e);
                    if(e instanceof FirebaseAuthInvalidCredentialsException){
                        //Log.d(TAG, "")
                    } else if(e instanceof FirebaseAuthUserCollisionException){

                    } else if(e instanceof FirebaseAuthInvalidUserException){

                    }
                }
            }
        });
    }

    private void updateUI(){
        // появление зелёной галочки(анимация)
        code.setEnabled(false);



        startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    private void initializeVerificationPhoneCallback(){
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onPhoneVerificationCompleted");
                linkWithCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.d(TAG, "onVerificationFailed:", e);

                if(e instanceof FirebaseAuthInvalidCredentialsException){
                    Log.d(TAG, "The request is malformed", e);
                    // неверный запрос

                } else if(e instanceof FirebaseTooManyRequestsException){
                    // смс квота для проекта превышена
                    Log.d(TAG, "SMS quota has been exceeded", e);
                } else if(e instanceof FirebaseApiNotAvailableException){
                    // на устройстве нет сервисов гугла
                    Log.d(TAG, "Device does not have Google Play Services", e);
                }
            }

            @Override
            public void onCodeSent(String _verificationId, PhoneAuthProvider.ForceResendingToken token){
                Log.d(TAG, "onCodeSent" + _verificationId);
                verificationId = _verificationId;
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String _verificationId){
                //Время верификации истекло

            }
        };
    }
}
