package org.apluscorp.publicpoll;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

/**
 * Created by Кирилл on 06.12.2017.
 */

public class AuthActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "Auth";
    @BindView(R.id.eMailInputField) EditText eMail;
    @BindView(R.id.passwordInputField) EditText password;
    private Tools tools;
    @BindView(R.id.scrollView) NestedScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.activitybar_auth);
        mAuth = FirebaseAuth.getInstance();

        tools = new Tools(this);
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if(!validateForm())
            return;

        final android.app.AlertDialog dialog = new SpotsDialog(this);
        dialog.setTitle("Вход в аккаунт...");
        dialog.show();

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    // вход завершён
                    Log.d(TAG, "signInWithEmail:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    startActivity(new Intent(AuthActivity.this, MainActivity.class));
                    dialog.dismiss();
                    finish();
                } else {
                    dialog.dismiss();
                    Exception exception = task.getException();
                    String exc = exception.getClass().toString();
                    Log.w(TAG, "signInWithEmail:failure" + exc, task.getException());
                    if(exc.endsWith("com.google.firebase.FirebaseNetworkException")) {
                        new AlertDialog.Builder(AuthActivity.this).setTitle("Ошибка входа в аккаунт")
                                .setMessage("Не удалось войти из-за проблем с интернет-соединением")
                                .setNegativeButton("Ок", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        }).create().show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
                        builder.setTitle("Ошибка входа в аккаунт")
                                .setMessage("Неправильный пароль или E-mail")
                                .setNegativeButton("Ок", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                        builder.create().show();
                    }
                }
            }
        });
    }

    private boolean validateForm() {
        boolean valid = true;
        String emailText = eMail.getText().toString();
        if(TextUtils.isEmpty(emailText)) {
            tools.setError(eMail, true);
            //eMail.setError("Обязательно для заполнения");
            valid = false;
        } else {
            tools.setError(eMail, false);
        }

        String passwordText = password.getText().toString();
        if(TextUtils.isEmpty(passwordText)) {
            tools.setError(password, true);
            valid = false;
        } else {
            tools.setError(password, false);
        }
        return valid;
    }

    public void onLoginButtonClick(View v) {
        signIn(eMail.getText().toString(), password.getText().toString());
    }

    public void onGoToRegistrationButtonClick(View v) {
        startActivity(new Intent(this, RegistrationActivity.class));
        eMail.setText("");
        password.setText("");
    }
}
