package org.apluscorp.publicpoll;

import android.app.ProgressDialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import dmax.dialog.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.net.URI;
import java.util.concurrent.TimeUnit;

/**
 * Created by Кирилл on 11.12.2017.
 */

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "Registration";
    @BindView(R.id.nameRegistrationField) EditText name;
    @BindView(R.id.eMailRegistrationField) EditText email;
    @BindView(R.id.passwordRegistrationField) EditText password;
    @BindView(R.id.confirmPasswordRegistrationField) EditText confPassword;
    @BindView(R.id.phoneRegistrationField) EditText phoneNumber;
    @BindView(R.id.checkBoxRegistrationField) CheckBox checked;
    @BindView(R.id.profile_image) ImageView profileImage;
    private Tools tool;
    private DatabaseReference userNames, newUserInfo;
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private Validator validator;
    private Uri selectedImage;
    static final int GALLERY_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        //startActivity(new Intent(this, FullVerificationDialogActivity.class));   // на время тестирования

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.activitybar_auth);

        mAuth = FirebaseAuth.getInstance();
        userNames = FirebaseDatabase.getInstance().getReference().child("userNames");
        tool = new Tools(this);

        validator = new Validator(this);
    }

    @OnTextChanged(R.id.nameRegistrationField)
    public void onNameRegistrationFieldTextChanged(CharSequence charSequence, int start, int before, int count) {
        Log.d(TAG, "Имя изменилось на " + charSequence);

        validator.validateName(name, userNames);
    }

    @OnTextChanged(R.id.eMailRegistrationField)
    public void onEMailRegistrationFieldTextChanged(CharSequence charSequence, int start, int before, int count) {
        Log.d(TAG, "Email изменился на " + charSequence);

        validator.validateEmail(email, mAuth);
    }

    @OnTextChanged(R.id.passwordRegistrationField)
    public void onPasswordRegistrationFieldTextChanged(CharSequence charSequence, int start, int before, int count) {
        validator.validatePassword(password);
    }

    private boolean validateForm() {
        boolean valid = true;

        valid = validator.validateName(name, userNames);
        valid &= validator.validateEmail(email, mAuth);
        valid &= validator.validatePhone(phoneNumber, true);
        valid &= validator.validateConfPassword(confPassword, password);
        valid &= validator.validateChecker(checked);

        return valid;
    }

    private void createAccount(final String email, final String password) {
        if(!validateForm()) {       // проверка валидации
            return;
        }

        final AlertDialog dialog = new SpotsDialog(this);
        dialog.setTitle("Регистрация...");
        dialog.show();

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "createUserWithEmail:success");
                    user = mAuth.getCurrentUser();
                    user.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(name.getText().toString()).build());
                    if(selectedImage != null) {
                        user.updateProfile(new UserProfileChangeRequest.Builder().setPhotoUri(selectedImage).build());
                    }

                    newUserInfo = FirebaseDatabase.getInstance().getReference();
                    newUserInfo.child("users").child(user.getUid()).child("email").setValue(email);
                    newUserInfo.child("users").child(user.getUid()).child("name").setValue(name.getText().toString());
                    userNames.child(name.getText().toString()).setValue(user.getUid());

                    Log.d(TAG, "createUserName:success");

                    user.sendEmailVerification().addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            dialog.dismiss();
                            if(task.isSuccessful()) {
                                Log.d(TAG, "sendEmailVerification:success");

                                if(phoneNumber.getText() != null){
                                    Intent verificationIntent = new Intent(RegistrationActivity.this, FullVerificationDialogActivity.class);
                                    verificationIntent.putExtra("phoneNumber", phoneNumber.getText().toString());
                                    startActivity(verificationIntent);
                                } else{
                                  startActivity(new Intent(RegistrationActivity.this, EmailVerificationDialogActivity.class));
                                }
                            } else {
                                Log.d(TAG, "sendEmailVerification:failure");
                                new AlertDialog.Builder(RegistrationActivity.this)
                                        .setTitle("")  // Добавить
                                        .setMessage("Регистрация завершена, однако не удалось отправить подтверждение на почту")
                                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.cancel();
                                                // ДАЛЬНЕЙШИЕ ДЕЙСТВИЯ
                                            }
                                        }).create().show();
                            }
                        }
                    });
                } else{
                    // fail
                    Exception e = task.getException();
                    /////// Обработка ошибок
                    Log.d(TAG, "createUserWithEmail:failure", e);
                    dialog.dismiss();
                    if(e instanceof FirebaseAuthUserCollisionException){
                        // пользователь с таким email уже существует
                        new AlertDialog.Builder(RegistrationActivity.this).setTitle("Регистрация не удалась")
                                .setMessage("Аккаунт с такой электронной почтой уже существует")
                                .setNegativeButton("Ок", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                }).create().show();
                    } else if(e instanceof FirebaseAuthInvalidCredentialsException){
                        // email адресс неверен
                    } else{

                    }
                }
            }
        });
    }

    public void onRegistrationButtonClick(View v) {
        createAccount(email.getText().toString(), password.getText().toString());
    }

    public void onProfileImageButtonClick(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if(requestCode == GALLERY_REQUEST) {
            if (resultCode != 0) {
                selectedImage = imageReturnedIntent.getData();
                profileImage.setImageURI(selectedImage);
            }
        }
    }

    private void createVerificationDialog(){
        startActivity(new Intent(this, FullVerificationDialogActivity.class));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "RegistrationActivity.onSaveInstanceState");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "RegistrationActivity.onPause");
    }
}
